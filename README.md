# Development Resources

A place to store common dev resources for `vscode` and `Visual Studio 2017`. Fonts, extension lists, snippets, notes, etc.

# VSCode Extensions

Helpful/cool/fun/productive extensions for `vscode`.

#### Development/Productivity

-  [dbaeumer.jshint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.jshint)
    -  The `jshint` plugin, hugely helpful, especially in `ES5` development.
-  [dbaeumer.eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
    -  The `eslint` plugin, great for `JavaScript` development.
-  [ms-vscode.sublime-keybindings](https://marketplace.visualstudio.com/items?itemName=ms-vscode.sublime-keybindings)
    -  Sublime keybindings. I find the sublime mappings more natural, having used it for years, but most importantly, use `ctrl` as your multi-cursor modifier!
-  [OmniSharp](https://github.com/OmniSharp/omnisharp-vscode)
   -  For C# in `vscode`
-  [Todo Tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree)
   -  A handy way of seeing remaining to-do's in your code.
   -  Pro Tip: Put this in your user settings file to exclude 3rd party or unwanted to-dos:
        ```json
            "todo-tree.excludeGlobs": [
                "*.min*", // Ignore any minified files. These shouldn't have to-do's anyway! But some do
                "*.map*", // Ignore any source maps

                // Paths generated for compiled output
                "**/bin/**",
                "**/obj/**",
                "**/Debug/**",

                // Ignore packages
                "**/packages/**",
                "**/node_modules/**",

                // Custom paths for our framework
                "bundle.js", // This is what we call certain bundled JS files (pre-minify)
                "*-bundle*",
                "*bundle-*",
                "**/OODAPI/Scripts/*.*", // 3rd party scripts for the OODAPI
                "**/Deploy/Release/**", // Staging of a release deploy
                "**/Deploy/Debug/**", // Staging of a debug deploy
            ],
        ```
-  [TODO Highlight](https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight)
   -  Don't miss your to-do's!
-  [Better Comments](https://github.com/aaron-bond/better-comments)
   -  Helpful for.... better comments!

#### Code Structure/Style

-  [streetsidesoftware.code-spell-checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
    -  Spell check all the things.
-  [HookyQR.beautify](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify)
    -  A great code beautifier.
-  [Better Align](https://marketplace.visualstudio.com/items?itemName=wwm.better-align)
   -  This. Is. Amazing. Just use it. Pro tip: Use `CTRL+K A` as the binding
-  [Trailing Spaces](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces)
   -  Trailing spaces bother me! This extension highlights them and lets you `destroy.them.all`. Pro tip: Use `CTRL+K X` as the binding. Pro tip 2: If you use two trailing spaces as line breaks in markdown... don't trim them with this!

#### Appearance

-  [dracula-theme.theme-dracula](https://draculatheme.com/)
    -  One of my favorite themes, can be installed in `Visual Studio 2017`, `Chrome DevTools`, and so many more.
-  [chireia.darker-plus-material-red](https://marketplace.visualstudio.com/items?itemName=chireia.darker-plus-material-red)
    -  A great dark/red/material theme that I currently use in `vscode`.
-  [evan-buss.font-switcher](https://marketplace.visualstudio.com/items?itemName=evan-buss.font-switcher)
    -  A fun way to quickly switch between fonts while looking at code.
 -  [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)
    -  Material Design Icons for Visual Studio Code

#### Documentation

-  [yzhang.markdown-all-in-one](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
    -  Helpful, additional markdown support in `vscode`.
-  [yzane.markdown-pdf](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf)
    -  Awesome plugin that can create a `PDF` from `markdown` incredibly quickly and easily. This is super helpful when someone wants some documentation, but they don't want markdown, e.g. a Help document sent to a customer.
 -  [Emoji Markdown Support](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-emoji)
    -  This is strictly just a fun addition, but sometimes an emoji is fun to throw in documentation.

#### Source Control

*  [Git Lens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
   *  Definitely needed if you're using `git`.
* [SVN by Chris Johnston](https://marketplace.visualstudio.com/items?itemName=johnstoncode.svn-scm)
  * I use `svn` in the corporate world, and [SVN by Chris Johnston](https://marketplace.visualstudio.com/items?itemName=johnstoncode.svn-scm) seems to be the best thing out there for that, but I haven't used it much :neutral_face:

# Snippets

```json
{
	// Default header in JavaScript files for jshint
	"JSHint Global Header": {
		"scope": "javascript,typescript",
		"prefix": "jshintglobals",
		"body": [
			"/* Common Globals     *//* globals window,document,$,jQuery,kendo */",
			"/* File-Level Globals *//* globals Some_Global_Function */"
		],
		"description": "Standard JavaScript globals header for JSHint"
	},

	// A slash-star comment block to be used in JavaScript + CSS
	"JavaScript Comment Block": {
		"scope": "javascript,typescript,css,scss",
		"prefix": "jscom",
		"body": [
			"/*****************",
			"\t$1",
			"*****************/",
		],
		"description": "Standard JavaScript Comment Block"
	},

	// For describing a JavaScript function parameter
	"JavaScript Parameter Definition": {
		"scope": "javascript,typescript",
		"prefix": "jsp",
		"body": [
			"@param {${1:argument data type}} ${2:argument name} - ${3:argument description}",
		],
		"description": "js param def"
	},

	// For creating an entire JavaScript class property
	"JavaScript Property Creator": {
		"scope": "javascript,typescript",
		"prefix": "cprop",
		"body": [
			"/**",
			" *\t@type {${1:type}} ${2:name} - ${3:description}",
			" */",
			"this.${2:name} = ${4:\"\"};$0"
		],
		"description": "Annotate a JavaScript class property"
	},

	// For describing a local JavaScript variable
	"Local JavaScript Variable Definition": {
		"scope": "javascript,typescript",
		"prefix": "jsvar",
		"body": [
			"/** @type {${1:type}} - ${2: description} */$0",
		],
		"description": "Describes a local JavaScript variable"
	},

	// The standard function summary for JavaScript
	"JavaScript Function Summary": {
		"scope": "javascript,typescript",
		"prefix": "jssumm",
		"body": [
			"/**",
			" * ${4:function summary}",
			" * @returns {${5:type}}",
			" * @param {${1:argument data type}} ${2:argument name} - ${3:argument description}",
			" */$0",
		],
		"description": "Standard JavaScript Function Summary"
	},

	// For describing a JavaScript property in a class
	"JavaScript Property Definition": {
		"scope": "javascript,typescript",
		"prefix": "jsprop",
		"body": [
			"/**",
			" *\t@property {${1:type}} ${2:name} - ${3:description}",
			" */",
		],
		"description": "Describes a JavaScript class property"
	},

	// For creating and describing a JavaScript prototype function with the var self = this; pattern
	"JavaScript Prototype Declaration": {
		"scope": "javascript,typescript",
		"prefix": "proto",
		"body": [
			"/**",
			" * ${3:func summary}$0",
			" */",
			"${1:class name}.prototype.${2:func name} = function(){",
			"\tvar self = this;",
			"};"
		],
		"description": "Stub for a JavaScript prototype function declaration"
	}
}
```

# Keyboard Shortcuts

*  **Better Align**
   *  `CTRL+K A` or `F1`
      *  I actually prefer `F1` because of how frequently I use this
*  **Trim Trailing Whitespace**
   *  `CTRL+K X`
*  **Go To Symbol In File** and **Go To Symbol In Workspace**
   * I swapped these commands because I found I was far more likely to go to symbol in my open file, rather than the whole workspace
   *  `CTRL+T` - Symbol in file
   *  `CTRL+SHIFT+O` - Symbol in workspace

# Linting

I often write `ES5` in an environment without transpiling options and if so, `jshint` is my go to. I also use `eslint`.

##### `jshint` Configuration

Install the package (https://www.npmjs.com/package/jshint). You'll need `node` and `npm`.

```
npm install --save jshint
```

In `vscode`, in User Settings, I define these defaults.

```json
"jshint.options": {
    "undef"         : true,
    "freeze"        : true,
    "futurehostile" : true,
    "curly"         : true, /* Opinion -- always wrap code in braces */
    "forin"         : true, /* Opinion -- prevent accidental references */
    "maxdepth"      : 5     /* Opinion -- less nested code is easier to manage */
}
```

# Themes

### `vscode`

I love the [Dracula](https://draculatheme.com/) theme and it works in both `vscode` and `Visual Studio 2017`, so it's a great choice if you want consistency between them. Dracula is awesome because it even has a theme for the Chrome Dev Tools, [you should check it out](https://chrome.google.com/webstore/detail/dracula-devtools-theme/gdhgkfojgddhijhlnnnbopleoabkeife?hl=en).

I have come to really love the [Darker+ Material Red](https://marketplace.visualstudio.com/items?itemName=chireia.darker-plus-material-red) `vscode` theme, too. Definitely worth checking out.

### `Visual Studio 2017`

*  [Dracula](https://draculatheme.com/visual-studio/)
   *  Install this first!
*  [Color Themes for Visual Studio](https://marketplace.visualstudio.com/items?itemName=VisualStudioPlatformTeam.ColorThemesforVisualStudio)
   *  Install this second! Pro tip: Use `One Dark Variant` for a nice looking theme

## Fonts

My two favorite fonts at the moment are [Hack](https://sourcefoundry.org/hack/) and [Inconsolata](https://levien.com/type/myfonts/inconsolata.html).

I jump around though, there is a `Fonts` directory in this project with downloads of various fonts I've tried. https://sourcefoundry.org/hack/playground.html is an awesome place to see some monospace fonts in action.

These are the fonts that currently in the `Fonts` directory in this project.

-  [Hack](https://sourcefoundry.org/hack/)
-  [Inconsolata](https://levien.com/type/myfonts/inconsolata.html)
-  [Fantasque Sans Mono](https://github.com/belluzj/fantasque-sans)
-  [Fira Mono](https://fonts.google.com/specimen/Fira+Mono)
-  [Source Code Pro](https://github.com/adobe-fonts/source-code-pro)
-  [Cousine](https://fonts.google.com/specimen/Cousine?selection.family=Cousine)

# Spell Checking

For `vscode` I use the [Code Spell Check by Street Side Software](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)

For `Visual Studio 2017` I use [Visual Studio Spell Checker by Eric Woodruff](https://marketplace.visualstudio.com/items?itemName=EWoodruff.VisualStudioSpellCheckerVS2017andLater)

# About

Austin Brown
[austinbrown2500@gmail.com](austinbrown2500@gmail.com) *(Personal)*  
[https://www.linkedin.com/in/ab2500](https://www.linkedin.com/in/ab2500)  
